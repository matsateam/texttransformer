package net.ukr.wumf;

public class UpDownTransformer extends TextTransformer{

	public UpDownTransformer() {
		super();
	}

	@Override
	public String transform(String text) {
		char[] t = text.toCharArray();
		String b ="";
		for (int i = 0; i < t.length; i++) {
			if(i%2==0) {
				t[i]=Character.toLowerCase(t[i]);
			}else t[i]=Character.toUpperCase(t[i]);
			b = b + t[i];
		} 
		return b;
	}

}
