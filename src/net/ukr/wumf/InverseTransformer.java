package net.ukr.wumf;

public class InverseTransformer extends TextTransformer {

	public InverseTransformer() {
		super();
	}

	@Override
	public String transform(String text) {
		StringBuilder x = new StringBuilder();
		x.append(text);
		String y = x.reverse().toString();
		return y;
	}

}
